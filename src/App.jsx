import React from 'react';
import { Feed } from './containers/Feed/Feed';
import { BrowserRouter } from 'react-router-dom';
import './styles/main.scss';

const App = () => {
  return (
    <BrowserRouter>
      <Feed />
    </BrowserRouter>
  );
};

export default App;
