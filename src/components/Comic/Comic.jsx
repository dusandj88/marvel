import React from 'react';
import PropTypes from 'prop-types';

export const Comic = ({ comic }) => {
  const { thumbnail, title } = comic;

  return (
    <div className="comic">
      <p className="title">{title}</p>
      <img src={`${thumbnail?.path}.${thumbnail.extension}`} />
    </div>
  );
};

Comic.propTypes = {
  comic: PropTypes.object,
};
