import React from 'react';
import { Comic } from '../Comic/Comic';
import PropTypes from 'prop-types';

export const Comics = ({ comics, searchedComics }) => {
  return (
    <div className="comics">
      {searchedComics.length === 0
        ? comics.map((comic, index) => <Comic key={index} comic={comic} />)
        : searchedComics.map((comic, index) => (
            <Comic key={index} comic={comic} />
          ))}
    </div>
  );
};

Comics.propTypes = {
  comics: PropTypes.array,
  searchedComics: PropTypes.array,
};
