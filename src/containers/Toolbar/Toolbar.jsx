import React from 'react';
import { useForm } from 'react-hook-form';
import { useHistory } from 'react-router-dom';

export const Toolbar = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();
  const history = useHistory();

  const onSubmit = (data) => {
    const dateRange = `dateRange=${data.starting_year}1%2C${data.ending_year}`;

    history.push({
      search: dateRange,
    });
  };

  return (
    <div className="toolbar">
      <p>
        Please enter valid DATE format (see placeholder for more information)
      </p>
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="toolbar__form_input">
          <label htmlFor="starting_year"></label>
          <input
            id="starting_year"
            {...register('starting_year', {
              required: 'This is required field',
            })}
            placeholder="2013-10-03"
          ></input>
          {errors.starting_year && (
            <p style={{ color: 'red' }}>{errors.starting_year.message}</p>
          )}
        </div>
        <div className="toolbar__form_input">
          <label htmlFor="ending_year"></label>
          <input
            id="ending-year"
            {...register('ending_year', { required: 'This is required field' })}
            placeholder="2020-03-01"
          ></input>
          {errors.ending_year && (
            <p style={{ color: 'red' }}>{errors.ending_year.message}</p>
          )}
        </div>
        <div>
          <input style={{ fontWeight: 'bold', width: '10rem' }} type="submit" />
        </div>
      </form>
    </div>
  );
};
