import React, { useState, useEffect } from 'react';
import { Comics } from '../../components/Comics/Comics';
import { Toolbar } from '../../containers/Toolbar/Toolbar';
import axios from 'axios';
import { useLocation } from 'react-router-dom';

export const Feed = () => {
  const [comics, setComics] = useState([]);
  const [searchTerm, setSearchTerm] = useState('');
  const [searchedComics, setSearchedComics] = useState([]);

  const location = useLocation();

  useEffect(() => {
    const APIEndpoint =
      location.search !== ''
        ? `http://gateway.marvel.com/v1/public/comics${location.search}&ts=1&apikey=8cb23b885d60ffe2dc23663c5d38893c&hash=028dac1b3d0722d4cc5152dba6b2397c`
        : `http://gateway.marvel.com/v1/public/comics?ts=1&apikey=8cb23b885d60ffe2dc23663c5d38893c&hash=028dac1b3d0722d4cc5152dba6b2397c`;
    axios
      .get(APIEndpoint)
      .then((response) => {
        setComics(response.data.data.results);
        setSearchTerm('');
        setSearchedComics([]);
      })
      .catch((error) => console.log('error', error.message));
  }, [location]);

  useEffect(() => {
    if (searchTerm !== ' ') {
      const filteredSearchResults = comics?.filter((comic) =>
        comic?.description?.includes(searchTerm),
      );
      setSearchedComics(filteredSearchResults);
    } else {
      setSearchedComics([]);
    }
  }, [searchTerm]);

  const handleSearchChange = (event) => {
    setSearchTerm(event.target.value);
  };

  return (
    <div className="feed">
      <Toolbar />
      <input
        style={{
          margin: '5rem 0 5rem 0',
          width: '30rem',
          display: 'flex',
          alignSelf: 'center',
        }}
        type="text"
        placeholder="Search"
        value={searchTerm}
        onChange={handleSearchChange}
      />
      <Comics comics={comics} searchedComics={searchedComics} />
    </div>
  );
};
